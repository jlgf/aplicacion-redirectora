#!/usr/bin/python

#
# Simple HTTP Server Random
# Jesus M. Gonzalez-Barahona
# jgb @ gsyc.es
# TSAI and SAT subjects (Universidad Rey Juan Carlos)
# September 2009
# Febraury 2022
#
# Returns an HTML page with a random link

import socket
import pokedata
import random


myPort = 1234
mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
mySocket.bind(('', myPort))

# Queue a maximum of 10 TCP connection requests

mySocket.listen(5)


try:
	while True:

		print("Waiting for connections")
		(recvSocket, address) = mySocket.accept()
		print("HTTP request received:")
		print(recvSocket.recv(2048))

		# Resource name for next url
		random.seed()
		pokemon_number = str(random.randint(1, len(pokedata.poke_dict)))
		selected_pokemon = pokedata.poke_dict.get(pokemon_number)
		nextUrl = "https://www.wikidex.net/wiki/" + selected_pokemon

		response = "HTTP/1.1 301 Moved Permanently\r\n" \
				   + "Location: " + nextUrl + "\r\n\r\n"

		recvSocket.send(response.encode('utf-8'))
		random.seed()
		recvSocket.close()

except KeyboardInterrupt:
	print("Closing binded socket")
	mySocket.close()